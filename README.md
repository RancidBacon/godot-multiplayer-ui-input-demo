### Godot Local Multiplayer UI Input Demo

![](images/multiplayer-ui-screenshot.png)

Three people at the same keyboard or (*new!*) with gamepads can control their specific `ItemList` UI control.

Implemented via a custom `InputFilterControl` class:

![](images/multiplayer-ui-node-tree-screenshot.png)

For more details see: `InputFilterControl.gd`

License: MIT
