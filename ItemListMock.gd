extends ItemList

func _ready():
    self.add_item("item 1")
    self.add_item("item 2")
    self.add_item("item 3")
    self.select(0)


func _on_ItemList_item_activated(index: int) -> void:

    $"../ActiveItemLabel".text = "Active: " + self.get_item_text(index)
